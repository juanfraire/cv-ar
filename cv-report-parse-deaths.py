# Matutino
report_mat = """
251 hombres
102 residentes en la provincia de Buenos Aires
23 residente en la Ciudad de Buenos Aires (CABA)
4 residente en la provincia de Chaco
1 residente en la provincia de Chubut
3 residentes en la provincia de Corrientes
21 residentes en la provincia de Córdoba
4 residentes en la provincia de Entre Ríos
19 residentes en la provincia de Jujuy
3 residentes en la provincia de La Rioja
15 residentes en la provincia de Mendoza
3 residentes en la provincia de Neuquén
7 residentes en la provincia de Río Negro
14 residentes en la provincia de Salta
2 residentes en la provincia de San Juan
16 residentes en la provincia de Santa Fe
1 residente en la provincia de Santiago del Estero
13 residentes en la provincia de Tucumán
198 mujeres
94 residentes en la provincia de Buenos Aires
23 residentes en la Ciudad de Buenos Aires (CABA)
4 residentes en la provincia de Chaco
26 residentes en la provincia de Córdoba
1 residente en la provincia de Entre Ríos
11 residentes en la provincia de Jujuy
13 residentes en la provincia de Mendoza
1 residente en la provincia de Neuquén
4 residentes en la provincia de Río Negro
4 residentes en la provincia de Salta
1 residente en la provincia de Santa Cruz
12 residentes en la provincia de Santa Fe
4 residentes en la provincia de Tucumán
"""
#report_mat = ""

# Vespertino
report_ves = """
"""

import re

def split(delimiters, string, maxsplit=0):
    import re
    regexPattern = '|'.join(map(re.escape, delimiters))
    return re.split(regexPattern, string, maxsplit)

provincias = [
    "Buenos Aires",
    "CABA",
    "Catamarca",
    "Chaco",
    "Chubut",
    "Córdoba",
    "Corrientes",
    "Entre Ríos",
    "Formosa",
    "Jujuy",
    "La Pampa",
    "La Rioja",
    "Mendoza",
    "Misiones",
    "Neuquén",
    "Río Negro",
    "Salta",
    "San Juan",
    "San Luis",
    "Santa Cruz",
    "Santa Fe",
    "Santiago del Estero",
    "Tierra del Fuego",
    "Tucumán"
]
fallecidos = [0] * len(provincias)

report = report_mat + report_ves

report = report.replace('  ', '. ')
report = report.replace('\n', '. ')

delimiters = "; y ", ", y ", "; ", ", ", ". ", "- ", " - ", "*"
report = split(delimiters, report)

for row in report:

    # if len(row)!=0:
    #     print(row)

    delimiters = " "
    row_split = split(delimiters, row)

    if len(row_split) > 2:

        if len(row_split[0]) == 0:
            row_split.remove(row_split[0])

        if row_split[1] == "residentes" or row_split[1] == "residente":

            if "provincia" in row_split:
                prov_index = row_split.index("provincia")
            if "Ciudad" in row_split or "ciudad" in row_split:
                prov_index = row_split.index("Buenos")

            # compose prov name
            prov_name = ""
            for index in range(prov_index + 2,len(row_split)):
                prov_name += row_split[index] + " " 

            # fix prov name
            if prov_name == "Santa Fé ":
                prov_name = "Santa Fe"
            if prov_name == "Santa fe ":
                prov_name = "Santa Fe"
            if prov_name == "La pampa ":
                prov_name = "La Pampa"
            if prov_name == "Tucuman ":
                prov_name = "Tucumán"

            # print(prov_name)

            # find prov and accumulate
            found = False
            for index, provincia in enumerate(provincias):
                if provincia in prov_name:

                    # check repeated
                    if found == True:
                        print("repeat: ", prov_name, provincia, row_split[0])
                        #continue

                    found = True
                    try:
                        fallecidos[index] += int(row_split[0])
                        # print("fallecidos:", row_split[0], "provincia:", prov_name)
                    except:
                        print("exception:", row, prov_name)
            if found == False:
                print("warning:", row, " - ", prov_name)

fallecidos_total = 0
for index, provincia in enumerate(provincias):
    # print(fallecidos[index], provincia)
    print(fallecidos[index])
    fallecidos_total += fallecidos[index]

print(fallecidos_total, "total")





        



